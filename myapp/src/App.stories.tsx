import React from "react";
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0";
import App from "./App";

interface Props {}
export default {
  title: "Customer/App",
  component: App,
} as Meta;

const Template: Story<Props> = (args) => <App {...args} />;

export const Primary = Template.bind({});
