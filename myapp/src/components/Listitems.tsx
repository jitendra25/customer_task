import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Avatar, ListItemAvatar, Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContentText from "@material-ui/core/DialogContentText";
import IconButton from "@material-ui/core/IconButton";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/icons/Close";
import Drawer from "@material-ui/core/Drawer";
import CustomDialog from "./CustomDialog";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
interface Props {}

interface User {
  name: string;
  email: string;
  phone?: string;
}
interface Props {
  users: User;
  deleteUser: Function;
  updateUser: Function;
}
const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    threee: {
      margin: "0px 5px",
      color: "#7A7A7A",
      cursor: "pointer",
    },
    three: {
      display: "flex",
      justifyContent: "flex-end",
      margin: "10px 20px 0px 0px",
    },
    drawerTitle: {
      display: "flex",
      alignItems: "center",
      margin: "30px 0 0 15px",
    },
    details: {
      padding: "0",
      marginLeft: "12px",
    },
    detailsp: {
      margin: "0px 10px",
    },

    paper: {
      width: "1000px",
      margin: "10px 20px",
      padding: "10px 20px",
    },
    textfield: {
      margin: "10px 0px",
    },
    action: {
      margin: "30px 10px",
    },
    cardText: {
      margin: "10px 0px",
    },
  })
);

const Listitemss: React.FC<Props> = ({ users, deleteUser, updateUser }) => {
  const [drawer, setDrawer] = React.useState(false);
  const [data, setData] = React.useState<User>({ name: "", email: "" });
  const [dDialog, setDialog] = React.useState(false);
  const [update, setUpdate] = React.useState(false);
  const classes = useStyle();
  function closeDrawer() {
    setDrawer(false);
  }
  const showDrawer = (value: User) => {
    setDrawer(true);
    setData(value);
  };
  const deletee = () => {
    setDialog(true);
  };
  const deleteee = () => {
    setDialog(false);
    deleteUser(users);
    setDrawer(false);
  };
  return (
    <div>
      <div>
        <ListItem onClick={() => showDrawer(users)} button>
          <ListItemAvatar>
            <Avatar
              style={{
                backgroundColor: "orange",
              }}
            >
              {users.name[0]}
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={users.name} secondary={users.email} />
        </ListItem>
        <Divider />
      </div>
      <div>
        <Drawer open={drawer} anchor={"right"} onClose={closeDrawer}>
          <div className={classes.three}>
            <IconButton onClick={() => setUpdate(true)}>
              <CreateOutlinedIcon />
            </IconButton>
            <IconButton onClick={deletee}>
              <DeleteOutlineIcon />
            </IconButton>
            <IconButton onClick={closeDrawer}>
              <CloseIcon />
            </IconButton>
          </div>
          <div className={classes.drawerTitle}>
            <Avatar
              style={{
                backgroundColor: "orange",
              }}
            >
              {data.name[0]}
            </Avatar>
            <div className={classes.details}>
              <p className={classes.detailsp} style={{ fontSize: "25px" }}>
                {data.name}
              </p>
              <p
                className={classes.detailsp}
                style={{
                  fontSize: "12px",
                  color: "#7A7A7A",
                  marginTop: "5px",
                }}
              >
                {data.email}
              </p>
            </div>
          </div>
          <div style={{ marginTop: "80px", marginLeft: "20px" }}>
            <p style={{ marginLeft: "20px", color: "#7A7A7A" }}>DETAILS</p>
            <Card elevation={3} className={classes.paper}>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.cardText}
              >
                Company
              </Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.cardText}
              >
                Address
              </Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.cardText}
              >
                Telephones
              </Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.cardText}
              >
                Langauge
              </Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.cardText}
              >
                Timezone
              </Typography>
            </Card>
          </div>
          <div style={{ marginLeft: "20px" }}>
            <p style={{ marginLeft: "20px", color: "#7A7A7A" }}>
              SPECIAL DATES
            </p>
            <Card elevation={3} className={classes.paper}>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.cardText}
              >
                Birth Date
              </Typography>
            </Card>
          </div>
        </Drawer>
      </div>
      <div>
        <Dialog open={dDialog} fullWidth>
          <DialogTitle>
            Delete Customer
            <DialogContentText style={{ marginTop: "30px" }}>
              Are you sure want to continue?
            </DialogContentText>
          </DialogTitle>

          <DialogActions style={{ margin: "30px" }}>
            <Button onClick={() => setDialog(false)}>Cancle</Button>
            <Button variant="contained" color="primary" onClick={deleteee}>
              CONFIRM
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <CustomDialog
        open={update}
        onClose={setUpdate}
        updateUser={updateUser}
        users={users}
        title={"Update Customer"}
      />
    </div>
  );
};
export default Listitemss;
