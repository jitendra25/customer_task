import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import CloseIcon from "@material-ui/icons/Close";
import SvgIcon from "@material-ui/core/SvgIcon";
import { ReactComponent as StarIcon } from "./india.svg";

const useStyle = makeStyles(() =>
  createStyles({
    threee: {
      margin: "0px 5px",
      color: "#7A7A7A",
      cursor: "pointer",
    },
    three: {
      display: "flex",
      justifyContent: "flex-end",
      margin: "10px 20px 0px 0px",
    },
    drawerTitle: {
      display: "flex",
      alignItems: "center",
      marginLeft: "15px",
    },
    details: {
      padding: "0",
      marginLeft: "12px",
    },
    detailsp: {
      margin: "0px 10px",
    },

    paper: {
      width: "1000px",
      margin: "10px 20px",
      padding: "10px 20px",
    },
    textfield: {
      margin: "10px 0px",
    },
    action: {
      margin: "30px 10px",
    },
  })
);
interface Props {
  open: boolean;
  onClose: Function;
  updateUser?: Function;
  users?: any;
  title: string;
  addUser?: Function;
}
const CustomDialog: React.FC<Props> = ({
  open,
  onClose,
  updateUser,
  users,
  title,
  addUser,
}) => {
  const [formData, updateFormData] = React.useState<any>({
    name: "",
    email: "",
    phone: " ",
  });

  const updateUs = (e: any) => {
    if (!formData.name.trim()) {
      setError({ ...error, ["name"]: "Enter the name" });
      return;
    } else if (!formData.email.trim()) {
      setError({ ...error, ["email"]: "Enter the email" });
      return;
    }
    if (updateUser !== undefined) updateUser(formData, users);
    else if (addUser !== undefined) addUser(formData);
    updateFormData({ name: "", email: "", phone: "" });
    onClose(false);
  };
  const [error, setError] = React.useState<any>({ name: "", email: "" });
  const classes = useStyle();

  const handleChange = (e: any) => {
    setError({ name: "", email: "" });
    updateFormData({ ...formData, [e.target.name]: e.target.value });
  };
  /* React.useEffect(() => {
    console.log("use Effect");
  }, []); */
  return (
    <div>
      <div>
        <Dialog open={open} fullWidth>
          <DialogActions>
            <CloseIcon
              onClick={() => {
                setError({ name: "", email: "" });
                onClose(false);
              }}
              style={{ cursor: "pointer" }}
            />
          </DialogActions>
          <DialogTitle>{title}</DialogTitle>
          <DialogContent>
            <TextField
              id="name"
              name="name"
              label="Name"
              className={classes.textfield}
              onChange={handleChange}
              fullWidth
            />
            <p style={{ color: "red", margin: "0" }}>{error.name}</p>

            <TextField
              name="email"
              label="Email"
              className={classes.textfield}
              onChange={handleChange}
              type="email"
              fullWidth
            />
            <p style={{ color: "red", margin: "0" }}>{error.email}</p>
            <Input
              name="phone"
              style={{ marginTop: "30px" }}
              type="phone"
              fullWidth
              onChange={handleChange}
              startAdornment={
                <InputAdornment position="start">
                  <SvgIcon component={StarIcon} viewBox="0 0 600 476.6" />
                  <pre> +91</pre>
                </InputAdornment>
              }
            />
            <DialogActions className={classes.action}>
              <Button
                onClick={() => {
                  setError({ name: "", email: "" });
                  onClose(false);
                }}
              >
                Cancle
              </Button>
              <Button variant="contained" color="primary" onClick={updateUs}>
                Save
              </Button>
            </DialogActions>
          </DialogContent>
        </Dialog>
      </div>
    </div>
  );
};
export default CustomDialog;
