import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import Listitemss from "./components/Listitems";
import Typography from "@material-ui/core/Typography";
import CustomDialog from "./components/CustomDialog";
interface Props {}

const useStyle: any = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "center",
      margin:'30px 0px',

    },
    search: {
      width: "200px",
      height: "40px",
      borderRadius: "50px",
      padding: "0px 8px",
      border: "1px solid #7A7A7A",
      display: "flex",
      alignItems: "center",
    },
    cont: {
      display: "flex",
      marginLeft: "auto",
    },
    head: {
      float: "left",
      fontWeight: "bold",
      fontSize: "25px",
    },
    main: {
      width: "80%",
      margin: "20px auto",
    },
    textfield: {
      margin: "10px 0px",
    },

    action: {
      margin: "30px 10px",
    },
  })
);
interface User {
  name: string;
  email: string;
  phone?: string;
}

const App: React.FC<Props> = () => {
  const [add, setAdd] = React.useState<boolean>(false);
  const [delet, setDe] = React.useState(false);
  const [user, addusers] = React.useState<User[]>([
    { name: "jitendra suthar", email: "jksuthar0225@gmail.com" },
    { name: "subhash gehlot", email: "subhash@example.com" },
    { name: "ramesh kumar", email: "email@example.com" },
  ]);

  const classes = useStyle();

  const showDialog = () => {
    setAdd(true);
  };
  function closee() {
    setAdd(false);
  }

  const addUser = (formData: User) => {
    let allUser: User[] = [...user];
    allUser.push(formData);
    closee();
    addusers(allUser);
  };

  const deleteUser = (index: User) => {
    var inn: number = user.indexOf(index);
    let temp: User[] = [...user];
    temp.splice(inn, 1);
    addusers(temp);
    setDe(true);
  };
  const updateUser = (updat: User, current: User) => {
    var inn: number = user.indexOf(current);
    let temp: User[] = [...user];
    temp[inn].name = updat.name;
    temp[inn].email = updat.email;
    addusers(temp);
  };
  function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const generate = () => {
    console.log("list called");
    return user.map((value, index) => {
      return (
        <Listitemss
          users={value}
          deleteUser={deleteUser}
          updateUser={updateUser}
        />
      );
    });
  };
  const memm = React.useMemo(() => generate(), [user]);
  return (
    <div className={classes.main}>
      <div className={classes.root}>
        <Typography variant="h5" >
          Customers
        </Typography>
        <div className={classes.cont}>
          <div className={classes.search}>
            
            <SearchIcon className={classes.icon} color="disabled" />
            <InputBase
              placeholder="Type to Search..."
              inputProps={{ "aria-label": "search google maps" }}
            />
          </div>
          <Fab
            color="default"
            size="small"
            style={{ marginLeft: "20px" }}
            onClick={showDialog}
          >
            <AddIcon color="disabled" />
          </Fab>
        </div>
      </div>
      <div>
        <List>{memm}</List>
      </div>
      <div>
        <CustomDialog
          open={add}
          onClose={closee}
          addUser={addUser}
          title={"New Customer"}
        />
      </div>
      <div>
        <Snackbar
          open={delet}
          autoHideDuration={6000}
          onClose={() => setDe(false)}
        >
          <Alert
            severity="info"
            onClose={() => setDe(false)}
            action={
              <Button
                size="small"
                style={{ color: "white" }}
                onClick={() => setDe(false)}
              >
                DISMISS
              </Button>
            }
          >
            Customer deleted successfully!
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
};
export default App;
